package org.jeecg.modules.demo.db.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.demo.db.entity.WebsocketMessages;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 消息表
 * @Author: jeecg-boot
 * @Date:   2021-08-11
 * @Version: V1.0
 */
@Mapper
public interface WebsocketMessagesMapper extends BaseMapper<WebsocketMessages> {
    @Select("select message from websocket_messages ORDER BY RAND() LIMIT 1")
    public String selectOne();
}
