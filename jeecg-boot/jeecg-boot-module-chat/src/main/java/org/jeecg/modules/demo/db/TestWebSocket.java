package org.jeecg.modules.demo.db;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jeecg.modules.demo.db.mapper.WebsocketMessagesMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import lombok.extern.slf4j.Slf4j;

/**
 * @Author scott
 * @Date 2019/11/29 9:41
 * @Description: 此注解相当于设置访问URL
 */
@Component
@Slf4j
@ServerEndpoint("/chat/websocket/{option}") //此注解相当于设置访问URL
public class TestWebSocket {
    private static WebsocketMessagesMapper tempMessages;
    @Autowired
    public void setTempMessages(WebsocketMessagesMapper tempMessages) {
        TestWebSocket.tempMessages = tempMessages;
    }
    private Session session;

    private String userId;

    String getRandomMsgFromFile() throws IOException {
        FileReader fr = new FileReader("C:\\Users\\you\\IdeaProjects\\chatproject\\src\\main\\resources\\msg.txt");
        BufferedReader br = new BufferedReader(fr);
        String line = "";
        String[] arrs = null;
        java.util.Random r = new java.util.Random();
        int RandomNum = Math.abs(r.nextInt()) % 10;
        String result = null;
        if ((line = br.readLine()) != null) {
            arrs = line.split(",");
            result = arrs[RandomNum];
        }
        br.close();
        fr.close();
        return result;
    }
    String getRandomMsgFromDatabase(){
        return tempMessages.selectOne();
    }
    String getRandomMsgFromAPI(String url) throws IOException, URISyntaxException {
        //通过get方式获得
        // 使用帮助类HttpClients创建CloseableHttpClient对象
        CloseableHttpClient httpclient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        // 创建uri
        URIBuilder builder = new URIBuilder(url);
        URI uri = builder.build();
        // 创建http GET请求实例
        HttpGet httpGet = new HttpGet(uri);
        // 执行请求
        response = httpclient.execute(httpGet);
        // 判断返回状态是否为200，即是否响应正常
        String result = "";
        if (response.getStatusLine().getStatusCode() == 200) {
            result = EntityUtils.toString(response.getEntity(), "UTF-8");
        }
        if (response != null) {
            response.close();
        }
        httpclient.close();
        return result;
    }

    /**
     * 缓存 webSocket连接到单机服务class中（整体方案支持集群）
     */
    private static CopyOnWriteArraySet<TestWebSocket> webSockets = new CopyOnWriteArraySet<>();
    private static Map<String, Session> sessionPool = new HashMap<String, Session>();


    @OnOpen
    public void onOpen(Session session, @PathParam(value = "userId") String userId) {
        try {
            this.session = session;
            this.userId = userId;
            webSockets.add(this);
            sessionPool.put(userId, session);
        } catch (Exception e) {
        }
    }

    @OnClose
    public void onClose() {
        try {
            webSockets.remove(this);
            sessionPool.remove(this.userId);
        } catch (Exception e) {
        }
    }

    /**
     * 服务器端推送消息
     */

    //发送消息的方法
    private synchronized void sendMsg(Session session, String msg) {
        try {
            session.getBasicRemote().sendText(msg);//阻塞型发送
        } catch (IOException e) {
            e.printStackTrace();//在命令行打印异常信息在程序中出错的位置及原因
        }
    }

    @OnMessage
    public void onMessage(String message, Session session,@PathParam("option") String option) throws IOException, URISyntaxException {
        String result = "";

        if (option.equals("FromFile")) {
            result = getRandomMsgFromFile();
        } else if (option.equals("FromAPI")) {
            result = getRandomMsgFromAPI("http://192.168.0.203:8000/chat?text=%E6%88%91%E6%98%AF%E8%B0%81");
        }
        else if(option.equals("FromDatabase")){
            result=getRandomMsgFromDatabase();
        }
//        for (TestWebSocket webSocket : webSockets) {
//            webSocket.pushMessage(result);
//        }
        sendMsg(session, result);
    }
    //异常
    @OnError
    public void onError(Session session, Throwable throwable) {
        System.out.println("出现异常");
        throwable.printStackTrace();//在命令行打印异常信息在程序中出错的位置及原因
    }
}
