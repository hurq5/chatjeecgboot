package org.jeecg.modules.demo.db.service.impl;

import org.jeecg.modules.demo.db.entity.WebsocketMessages;
import org.jeecg.modules.demo.db.mapper.WebsocketMessagesMapper;
import org.jeecg.modules.demo.db.service.IWebsocketMessagesService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 消息表
 * @Author: jeecg-boot
 * @Date:   2021-08-11
 * @Version: V1.0
 */
@Service
public class WebsocketMessagesServiceImpl extends ServiceImpl<WebsocketMessagesMapper, WebsocketMessages> implements IWebsocketMessagesService {

}
