package org.jeecg.modules.demo.db.service;

import org.jeecg.modules.demo.db.entity.WebsocketMessages;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 消息表
 * @Author: jeecg-boot
 * @Date:   2021-08-11
 * @Version: V1.0
 */
public interface IWebsocketMessagesService extends IService<WebsocketMessages> {

}
